package com.etna.matchservice.domain;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by chris on 25/08/2017.
 */
public class LikeId implements Serializable {
    UUID fromUser;
    UUID toUser;
}
