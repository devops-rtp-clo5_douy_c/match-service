package com.etna.matchservice.repository;


import com.etna.matchservice.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by chris on 16/08/2017.
 */
@Repository
public interface MatchRepository extends JpaRepository<Match, UUID> {

    List<Match> findByFirstUserIdOrSecondUserId(String fUserId, String sUserId);

    @Query("select m from Match m " +
            "where (m.firstUserId = ?1 and m.secondUserId = ?2) " +
            "or (m.firstUserId = ?2 and m.secondUserId = ?1) ")
    List<Match> findMatchesBetweenUsers(UUID firstUser, UUID secondUser);

}
