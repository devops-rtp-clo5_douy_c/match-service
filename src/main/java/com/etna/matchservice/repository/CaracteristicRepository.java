package com.etna.matchservice.repository;

import com.etna.matchservice.domain.Caracteristic;
import com.etna.matchservice.domain.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by chris on 16/08/2017.
 */
@Repository
public interface CaracteristicRepository extends JpaRepository<Caracteristic, UUID> {

    List<Caracteristic> findByBirthDateBetweenAndGender(LocalDate dateFrom, LocalDate dateTo, boolean gender);
}
