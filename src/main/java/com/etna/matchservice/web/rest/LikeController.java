package com.etna.matchservice.web.rest;

import com.etna.matchservice.domain.Like;
import com.etna.matchservice.domain.Match;
import com.etna.matchservice.repository.LikeRepository;
import com.etna.matchservice.repository.MatchRepository;
import com.etna.matchservice.web.rest.dto.LikeItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


/**
 * Created by chris on 16/08/2017.
 */
@Controller
@RequestMapping("/devops-match/1.0.0")
public class LikeController {

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private MatchRepository matchRepository;

    @RequestMapping(value="/like/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public List<LikeItem> getLikes(@PathVariable UUID userId){
        return likeRepository.findByUserId(userId);
    }

    /**
     * Add a like from a user to another
     * A like can be changed over time (but won't remove an already existing match)
     * Reciprocal like creates a match
     * @param userId
     * @param like
     * @return
     */
    @RequestMapping(value="/like/{userId}", method = RequestMethod.POST)
    public ResponseEntity addLike(@PathVariable UUID userId, @RequestBody LikeItem like){
        Like l = new Like(userId, like.getUserId(), like.isStatus());
        if (likeRepository.findOne(Example.of(l)) != null)
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        likeRepository.save(l);
        //reciprocal like creates a match
        if (l.isStatus()
                && likeRepository.findOne(Example.of(new Like(l.getToUser(), l.getFromUser(), true))) != null
                && matchRepository.findMatchesBetweenUsers(l.getFromUser(), l.getToUser()).size() < 1) {
            matchRepository.save(new Match(l.getFromUser(), l.getToUser()));
        }
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
