package com.etna.matchservice.repository;

import com.etna.matchservice.domain.Like;
import com.etna.matchservice.domain.LikeId;
import com.etna.matchservice.web.rest.dto.LikeItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by chris on 16/08/2017.
 */
@Repository
public interface LikeRepository extends JpaRepository<Like, LikeId> {
    @Query(value = "select new com.etna.matchservice.web.rest.dto.LikeItem(l.toUser, l.status) from Like l where l.fromUser = ?1")
    List<LikeItem> findByUserId(UUID fromUserId);
}
