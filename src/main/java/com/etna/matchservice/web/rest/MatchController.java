package com.etna.matchservice.web.rest;

import com.etna.matchservice.domain.Match;
import com.etna.matchservice.repository.LikeRepository;
import com.etna.matchservice.repository.MatchRepository;
import com.etna.matchservice.util.SkipOffsetPageable;
import com.etna.matchservice.web.rest.dto.LikeItem;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


/**
 * Created by chris on 16/08/2017.
 */
@Controller
@RequestMapping("/devops-match/1.0.0")
public class MatchController {

    @Autowired
    private MatchRepository matchRepository;

    @RequestMapping(value="/match", method = RequestMethod.GET)
    @ResponseBody
    public List<Match> getMatches(@RequestParam(required = false) Integer skip, @RequestParam(required = false) Integer limit){
        Page<Match> p = matchRepository.findAll(new SkipOffsetPageable(skip, limit));
        return p != null ? p.getContent() : null;
    }

    /**
     * We're assuming matches are bi-directional, if A matches B, then it means B also matches A (for the same entry line)
     * @param userId
     * @return
     */
    @RequestMapping(value="/match/{userId}", method = RequestMethod.GET)
    public List<Match> getMatches(@PathVariable String userId){
        return matchRepository.findByFirstUserIdOrSecondUserId(userId, userId);
    }

    /**
     * The doc doesn't specify what happens when a match between two users already exists
     * Since we have a match_id and we're not using their user_id as PK we'll just add a new match with the current date
     * @param match
     * @return
     */
    @RequestMapping(value="/match", method = RequestMethod.POST)
    public ResponseEntity<Match> createMatch(@RequestBody Match match){
        match.setMatchDate(LocalDateTime.now());
        matchRepository.save(match);
        return new ResponseEntity<Match>(HttpStatus.OK);
    }
}
