package com.etna.matchservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by chris on 25/08/2017.
 */
@Entity
@Data
@NoArgsConstructor
public class Match {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "BINARY(16)")
    @JsonProperty("match_id")
    private UUID matchId;

    @JsonProperty("first_user_id")
    private UUID firstUserId;

    @JsonProperty("second_user_id")
    private UUID secondUserId;

    @JsonProperty("match_date")
    private LocalDateTime matchDate = LocalDateTime.now();

    public Match(UUID firstUserId, UUID secondUserId) {
        this.firstUserId = firstUserId;
        this.secondUserId = secondUserId;
    }
}
