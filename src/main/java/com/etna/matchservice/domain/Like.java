package com.etna.matchservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created by chris on 16/08/2017.
 */
@Entity
@Table(name = "\"like\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(LikeId.class)
public class Like {

    @Id @NonNull private UUID fromUser;
    @Id @NonNull private UUID toUser;
    @NonNull private boolean status;

}
