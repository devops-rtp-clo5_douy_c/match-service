Match microservice

A git push triggers the following :

1 -- Building the application (catch compile time errors asap). The compilation is done on a basic alpine image with Java8, and could also be done with the current image of the microservice

2 -- Building the Docker image (adding the source code and the .jar previously compiled on the image). This image will be pushed in the registry as a TEST_IMAGE, in order to avoid any impact on the current PROD image (aka latest)
The image currently only consists of an Alpine image with Java 8 installed.

3 -- Running the application tests on the Docker image (This ensures that both the code and the Docker Image are running just fine)
For this service we're only doing a few unit tests as an example
See Inbox Service for an Integration Test (requires full context of the application in order to fully interact with the DB)
Theory would be to first run the unit tests (faster to execute) then the full integration tests (complex use cases/specific prod-like configuration). 

IF pushing on master : 

4 -- Pushing the previously created "TEST" image in the registry as "latest"

5 -- Deploying the application on Google Cloud with Kubernetes (Match service should be on http://35.195.17.180:8080 + route, see the "Environments")