package com.etna.matchservice.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;

import java.util.UUID;

/**
 * Created by chris on 25/08/2017.
 */
@Data
public class ParameterItem {
    @JsonProperty("age_max")
    private int ageMax;

    @JsonProperty("age_min")
    private int ageMin;

    private boolean gender;
}
