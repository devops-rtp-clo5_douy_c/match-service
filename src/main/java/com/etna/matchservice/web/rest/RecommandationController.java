package com.etna.matchservice.web.rest;

import com.etna.matchservice.domain.Caracteristic;
import com.etna.matchservice.repository.CaracteristicRepository;
import com.etna.matchservice.web.rest.dto.ParameterItem;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;


/**
 * Created by chris on 16/08/2017.
 */
@Controller
@RequestMapping("/devops-match/1.0.0")
public class RecommandationController {

    @Autowired
    private CaracteristicRepository caracRepository;

    @RequestMapping(value="/recommandation", method = RequestMethod.POST)
    public List<Caracteristic> getRecommandations(@RequestBody ParameterItem parameterItem){
        LocalDate from = LocalDate.now().minusYears(parameterItem.getAgeMax());
        LocalDate to = LocalDate.now().minusYears(parameterItem.getAgeMin());
        return caracRepository.findByBirthDateBetweenAndGender(from, to, parameterItem.isGender());
    }

    @RequestMapping(value="/recommandation/{userId}/caracteristic", method = RequestMethod.GET)
    public Caracteristic getCaracteristic(@PathVariable UUID userId){
        return caracRepository.findOne(userId);
    }

    @RequestMapping(value="/recommandation/{userId}/caracteristic", method = RequestMethod.PUT)
    public ResponseEntity<Caracteristic> updateCaracteristic(@PathVariable UUID userId, @RequestBody Caracteristic caracteristic){
        Caracteristic caracDB = caracRepository.findOne(caracteristic.getUserId());
        if (caracDB == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        BeanUtils.copyProperties(caracteristic, caracDB);
        return new ResponseEntity<>(caracRepository.save(caracDB), HttpStatus.OK);
    }

    @RequestMapping(value="/recommandation/{userId}/caracteristic", method = RequestMethod.POST)
    public ResponseEntity<Caracteristic> createCaracteristic(@RequestBody Caracteristic carac){
        if (caracRepository.findOne(carac.getUserId()) != null)
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        return new ResponseEntity(caracRepository.save(carac), HttpStatus.CREATED);
    }
}
