package com.etna.matchservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by chris on 25/08/2017.
 */
@Entity
@Data
public class Caracteristic {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "BINARY(16)")
    @JsonProperty("user_id")
    private UUID userId;

    @JsonProperty("first_user_id")
    private String bio;

    @JsonProperty("birthDate")
    private LocalDate birthDate;

    //not sure what the value means here
    private boolean gender;

    @OneToOne
    @PrimaryKeyJoinColumn
    private Photo photo;

}
