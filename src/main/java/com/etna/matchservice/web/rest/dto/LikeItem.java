package com.etna.matchservice.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;

import java.util.UUID;

/**
 * Created by chris on 25/08/2017.
 */
@Data
public class LikeItem {
    @JsonProperty("user_id")
    @NonNull private final UUID userId;
    @NonNull private final boolean status;
}
