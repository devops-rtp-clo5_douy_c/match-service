package com.etna.matchservice;

import com.etna.matchservice.repository.LikeRepository;
import com.etna.matchservice.repository.MatchRepository;
import com.etna.matchservice.web.rest.LikeController;
import com.etna.matchservice.web.rest.MatchController;
import com.etna.matchservice.web.rest.dto.LikeItem;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(LikeController.class)
public class MatchServiceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LikeRepository likeRepository;

    @MockBean
    private MatchRepository matchRepository;

    @Test
    /**
     * Mocks 10 likes, 5 being true and 5 being false, and assert they're all properly found
     */
    public void findAllLikesFromUser_ShouldReturnFoundLikes() throws Exception {
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .build();
        List<LikeItem> likes = new ArrayList<>();
        for (int i=0; i < 10; i++) {
            likes.add(
                   new LikeItem(UUID.randomUUID(), i%2==0)
            );
        }
        UUID fromUserId = UUID.randomUUID();
        Mockito.when(
                likeRepository.findByUserId(fromUserId)
        ).thenReturn(likes);

        mockMvc.perform(get("/devops-match/1.0.0/like/{userId}", fromUserId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].status", is(true)))
                .andExpect(jsonPath("$[1].status", is(false)));

        Mockito.verify(likeRepository, Mockito.times(1)).findByUserId(Mockito.any(UUID.class));
        Mockito.verifyNoMoreInteractions(likeRepository);
    }

}
